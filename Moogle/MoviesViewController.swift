//
//  ViewController.swift
//  Moogle
//
//  Created by David Velarde on 5/11/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import Moya_Gloss
import ChameleonFramework

class MoviesViewController: MoogleViewController {
    
    @IBOutlet weak var tvMovies: UITableView!
    
    
    
    var arrayMovies = [Movie]()
    var page = 1
    var isLoading = false
    
    override func viewDidLoad() {
        icon = "🎬"
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationItem.backBarButtonItem?.title = "🎬"
        
        tvMovies.estimatedRowHeight = 80
        tvMovies.rowHeight = UITableViewAutomaticDimension
        loadNewData()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.hidesNavigationBarHairline = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadNewData(){
        if !isLoading{
            isLoading = true
            let _ = traktProvider.request(TraktService.listPopular(page: page)) { result in
                self.isLoading = false
                switch result {
                case let .success(moyaResponse):
                    if let newData = try? moyaResponse.mapArray(Movie.self) {
                        self.addNewMoviesToTable(newData: newData)
                        self.page+=1
                    }
                case let .failure:
                    showError(.connection)
                }
            }
        }
    }
    
    func addNewMoviesToTable(newData:[Movie]){
        arrayMovies.append(contentsOf: newData)
        tvMovies.reloadData()   
    }

}

extension MoviesViewController : UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMovies.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == arrayMovies.count {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell") {
                let activity : UIActivityIndicatorView = cell.viewWithTag(1) as! UIActivityIndicatorView
                activity.isHidden = false
                activity.startAnimating()
                return cell
            }
            
        }
        else{
            let currentMovie = arrayMovies[indexPath.row]
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell") as? MovieTableViewCell {
            
                cell.prepare(forMovie: currentMovie)
                cell.currentIndexPath = indexPath
                return cell
            }
        }
        return UITableViewCell()
    }
}

extension MoviesViewController : UITableViewDelegate{

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == arrayMovies.count {
            
            loadNewData()
        
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedMovie = arrayMovies[indexPath.row]
        performSegue(withIdentifier: "goToDetail", sender: self)
    }
}

