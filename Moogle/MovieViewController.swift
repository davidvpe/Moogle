//
//  MovieViewController.swift
//  Moogle
//
//  Created by David Velarde on 5/12/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import Nuke
import ChameleonFramework
class MovieViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tvOverview: UITextView!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgMovie: UIImageView!
    var selectedMovie : Movie?
    
    override func viewDidLoad() {
        self.title = "Movie Detail"
        
        super.viewDidLoad()
        if let movie = self.selectedMovie {
            if let url = movie.posterURL {
                Nuke.loadImage(with: url, into: imgMovie)
            }
            lblTitle.text = movie.title + " (" + String(movie.year) + ")"
            tvOverview.text = movie.overview
            viewBackground.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: viewBackground.frame, andColors: [UIColor.clear,UIColor.gray])
        }
        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
