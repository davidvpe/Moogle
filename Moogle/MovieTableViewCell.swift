//
//  MovieTableViewCell.swift
//  Moogle
//
//  Created by David Velarde on 5/12/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import Nuke

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblOverview: UILabel!
    var isLoadingPicture = false
    var currentIndexPath : IndexPath!
    var currentMovie : Movie!
    
    func prepare(forMovie movie:Movie) {
        imgPoster.image = nil
        movie.gotURL = nil
        currentMovie = movie
        lblTitle.text = movie.title + " (" + String(movie.year) + ")"
        lblOverview.text = movie.overview
        if let url = movie.posterURL {
            print("Downloading Image for URL: \(url.absoluteString)")
            Nuke.loadImage(with: url, into: imgPoster)
        }
        else{
            movie.gotURL = {
                url in
                if movie.tmdbId == self.currentMovie.tmdbId {
                    print("Downloading Image FROM CLOSURE for URL: \(url.absoluteString)")
                    Nuke.loadImage(with: url, into: self.imgPoster)
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
