//
//  MoogleViewController.swift
//  Moogle
//
//  Created by David Velarde on 5/12/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

class MoogleViewController: UIViewController {

    
    var selectedMovie : Movie!
    var icon = "🎬"
    override func viewDidLoad() {
        super.viewDidLoad()

        let barButton = UIBarButtonItem(title: icon, style: .plain, target: self, action: #selector(goBack))
        
        self.navigationItem.backBarButtonItem = barButton
        
        // Do any additional setup after loading the view.
        
        self.navigationController?.hidesNavigationBarHairline = true
    }
    
    func goBack(){
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "goToDetail" {
            if let vc = segue.destination as? MovieViewController {
                vc.selectedMovie = selectedMovie
            }
        }
    }

}
