//
//  MovieDB.swift
//  Moogle
//
//  Created by David Velarde on 5/12/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import RealmSwift

class MovieDB: Object {
    dynamic var tmdbId = 0
    dynamic var posterURL = ""
    
    override static func primaryKey() -> String? {
        return "tmdbId"
    }

    
}
