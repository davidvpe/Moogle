//
//  TraktService.swift
//  Moogle
//
//  Created by David Velarde on 5/11/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import Moya


let tmdbProvider = MoyaProvider<TMDBService>()

enum TMDBService {
    case getImages(movieId:Int)
}

extension TMDBService: TargetType {
    var baseURL : URL { return URL(string: "https://api.themoviedb.org/3")! }
    var path : String {
        switch self {
        case .getImages(let movieId):
            return "/movie/\(movieId)/images"
        }
    }
    
    var method : Moya.Method {
        switch self {
        case .getImages:
            return .get
        }
    }
    var parameters: [String: Any]? {
        switch self {
        case .getImages:
            return [
                "api_key":"599309b33b0e3f7015d08b38063e0505",
                "language":"en-US",
                "include_image_language":"en,null"
            ]
        }
    }
    var parameterEncoding : ParameterEncoding {
        switch self {
        case .getImages:
            return URLEncoding.default
        }
    }
    var sampleData : Data {
        switch self {
        case .getImages:
            return "".utf8Encoded
        }
    }
    var task : Task {
        return .request
    }
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return self.data(using: .utf8)!
    }
}
