//
//  TraktService.swift
//  Moogle
//
//  Created by David Velarde on 5/11/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import Moya


let traktProvider = MoyaProvider<TraktService>(endpointClosure: endpointClosure)

let endpointClosure = { (target : TraktService) -> Endpoint<TraktService> in
    
    let defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
    
    return defaultEndpoint.adding(newHTTPHeaderFields: [
        "trakt-api-key":"47a36bffa406fec1f7193c0f40e98f5eeb8dbb798ae1c61f0a533bb7ddb4418d",
        "trakt-api-version":"2"])
}

enum TraktService {
    case listPopular(page:Int)
    case search(query:String, page:Int)
}

extension TraktService: TargetType {
    var baseURL : URL { return URL(string: "https://api.trakt.tv")! }
    var path : String {
        switch self {
        case .listPopular:
            return "/movies/popular"
        case .search:
            return "/search/movie"
        }
    }
    
    var method : Moya.Method {
        switch self {
        case .listPopular,.search:
            return .get
        }
    }
    var parameters: [String: Any]? {
        switch self {
        case .listPopular(let page):
            return ["extended":"full","page":page,"limit":10]
        case .search(let query, let page):
            return ["query":query,"extended":"full","page":page,"limit":10]
        }
    }
    var parameterEncoding : ParameterEncoding {
        switch self {
        case .listPopular,.search:
            return URLEncoding.default
        }
    }
    var sampleData : Data {
        switch self {
        case .listPopular:
            return "".utf8Encoded
        case .search:
            return "".utf8Encoded
        }
    }
    var task : Task {
        return .request
    }
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return self.data(using: .utf8)!
    }
}
